/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 */

import React, { useEffect, memo } from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import styled from 'styled-components';

import { useInjectReducer } from 'utils/injectReducer';
import { useInjectSaga } from 'utils/injectSaga';
import {
  makeSelectRepos,
  makeSelectLoading,
  makeSelectError,
} from 'containers/App/selectors';
import HeadLiner from 'components/HeadLiner';
import Card from 'components/Card';
import Deck from 'components/Deck';
import { GiLaurelsTrophy } from 'react-icons/gi';
import { loadRepos } from '../App/actions';
import { changeUsername } from './actions';
import { makeSelectUsername } from './selectors';
import reducer from './reducer';
import saga from './saga';

GiLaurelsTrophy;

const key = 'home';

const ActivityCard = styled.div`
  background: #1a2739;
  border: 1px #1e2a3c solid;
  padding: 16px;
  color: #fff;
  border-radius: 8px;
  font-family: 'Segoe UI', Frutiger, 'Frutiger Linotype', 'Dejavu Sans',
    'Helvetica Neue', Arial, sans-serif;
  margin: 18px 18px;

  p {
    font-family: 'Segoe UI', Frutiger, 'Frutiger Linotype', 'Dejavu Sans',
      'Helvetica Neue', Arial, sans-serif;
  }
`;

export function TournamentPage({
  username,
  loading,
  error,
  repos,
  onSubmitForm,
  onChangeUsername,
}) {
  useInjectReducer({ key, reducer });
  useInjectSaga({ key, saga });

  useEffect(() => {
    // When initial state username is not null, submit the form to load repos
    if (username && username.trim().length > 0) onSubmitForm();
  }, []);

  const reposListProps = {
    loading,
    error,
    repos,
  };

  return (
    <article style={{ flex: '1' }}>
      <Helmet>
        <title>Tournaments</title>
        <meta
          name="description"
          content="A React.js Boilerplate application homepage"
        />
      </Helmet>
      <HeadLiner>Live Tournaments</HeadLiner>
      <Deck>
        <Card
          title="Wild Tornado"
          prizePool="2000"
          gameMode="score"
          image="https://i.imgur.com/V6Lqdzo.png"
        />
        <Card
          title="Wishmaster"
          prizePool="500"
          gameMode="multiplier"
          image="https://i.imgur.com/KqjXxqm.png"
        />
        <Card
          title="Starburst"
          prizePool="2000"
          gameMode="score"
          image="https://i.imgur.com/KcFmzML.png"
        />
        <Card
          title="Space Wars"
          prizePool="1000"
          gameMode="multiplier"
          image="https://i.imgur.com/8mSi1Pa.png"
        />
      </Deck>
      <HeadLiner>Upcoming Tournaments</HeadLiner>
      <Deck>
        <Card
          title="Jungle Spirit"
          prizePool="1250"
          gameMode="score"
          image="https://i.imgur.com/CfeDwec.png"
        />
        <Card
          title="Koi Princess"
          prizePool="250"
          gameMode="multiplier"
          image="https://i.imgur.com/kQ6ZIIb.png"
        />
        <Card
          title="Narcos"
          prizePool="500"
          gameMode="score"
          image="https://i.imgur.com/7GBQRUM.png"
        />
        <Card
          title="Reel Rush"
          prizePool="3000"
          gameMode="multiplier"
          image="https://i.imgur.com/VukN0g6.png"
        />
      </Deck>
      <HeadLiner>Recent Activities</HeadLiner>
      <Deck>
        <ActivityCard>Text</ActivityCard>
      </Deck>
    </article>
  );
}

TournamentPage.propTypes = {
  loading: PropTypes.bool,
  error: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
  repos: PropTypes.oneOfType([PropTypes.array, PropTypes.bool]),
  onSubmitForm: PropTypes.func,
  username: PropTypes.string,
  onChangeUsername: PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
  repos: makeSelectRepos(),
  username: makeSelectUsername(),
  loading: makeSelectLoading(),
  error: makeSelectError(),
});

export function mapDispatchToProps(dispatch) {
  return {
    onChangeUsername: evt => dispatch(changeUsername(evt.target.value)),
    onSubmitForm: evt => {
      if (evt !== undefined && evt.preventDefault) evt.preventDefault();
      dispatch(loadRepos());
    },
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(TournamentPage);
