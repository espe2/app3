/**
 *
 * App
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 */

import React from 'react';
import { Helmet } from 'react-helmet';
import styled from 'styled-components';
import { Switch, Route } from 'react-router-dom';

import HomePage from 'containers/HomePage/Loadable';
import DiamondLeaguePage from 'containers/DiamondLeaguePage/Loadable';
import NewsPage from 'containers/NewsPage/Loadable';
import BonusesPage from 'containers/BonusesPage/Loadable';
import FeaturePage from 'containers/FeaturePage/Loadable';
import TournamentsPage from 'containers/TournamentPage/Loadable';
import NotFoundPage from 'containers/NotFoundPage/Loadable';
import TopBar from 'components/TopBar';
import FriendsPanel from 'components/FriendsPanel';
import IconMenu from 'components/IconMenu';
import TextMenu from 'components/TextMenu';
import Footer from 'components/Footer';
import Img from 'components/Img';
import EventFeed from 'components/EventFeed';

import GlobalStyle from '../../global-styles';

import Background from '../../images/background.jpg';
import Hero from '../../images/hero.png';

const AppWrapper = styled.div`
  background: rgb(0, 16, 41);
  background: linear-gradient(
    45deg,
    rgba(20, 34, 53, 0.95) 0%,
    rgba(5, 25, 45, 0.95) 100%
  );
  flex: 1;
  display: flex;
  flex-direction: column;
  justify-content: center;
  z-index: 99;
`;

const ConentWrapper = styled.div`
  display: flex;
  flex: 1;
  flex-direction: row;
  justify-content: flex-start;
`;

const Holder = styled.div`
  height: 100%;
  min-height: 100%;
  display: flex;
  flex-direction: column;
`;

const BackgroundContainer = styled.div`
  img {
    position: fixed;
    left: 0;
    right: 0;
    top: 0;
    bottom: 0;
    z-index: 1;
    width: 100%;
    height: 100%;
  }
`;

const HeroContainer = styled.div`
  img {
    position: fixed;
    left: 10px;
    bottom: 10px;
    z-index: 9999;
    width: 160px;
  }
`;

const ScrollContainer = styled.div`
  flex:1;
  flex-direction: column;
  overflow-y:scroll;

  ::-webkit-scrollbar{width:10px}
  ::-webkit-scrollbar-track{background:rgb(255,183,0,0.1);}
  ::-webkit-scrollbar-thumb{background:rgb(255,183,0,0.4);}</style>
`;

export default function App() {
  return (
    <Holder>
      <BackgroundContainer>
        <Img src={Background} />
      </BackgroundContainer>
      <HeroContainer>
        <Img src={Hero} />
      </HeroContainer>
      <EventFeed />
      <AppWrapper>
        <Helmet
          titleTemplate="%s - Reelzone"
          defaultTitle="React.js Boilerplate"
        >
          <meta
            name="description"
            content="A React.js Boilerplate application"
          />
        </Helmet>
        <TopBar />
        <ConentWrapper>
          <IconMenu />
          <TextMenu />
          <ScrollContainer style={{ height: window.innerHeight - 80 }}>
            <Switch>
              <Route exact path="/" component={HomePage} />
              <Route path="/tournament/starburst" component={FeaturePage} />
              <Route path="/tournaments" component={TournamentsPage} />
              <Route path="/league" component={DiamondLeaguePage} />
              <Route path="/bonuses" component={BonusesPage} />
              <Route path="/news" component={NewsPage} />
              <Route path="" component={NotFoundPage} />
            </Switch>
          </ScrollContainer>
          <FriendsPanel />
        </ConentWrapper>
        <GlobalStyle />
      </AppWrapper>
    </Holder>
  );
}
