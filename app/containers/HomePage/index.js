/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 */

import React, { useEffect, memo } from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import styled from 'styled-components';

import { useInjectReducer } from 'utils/injectReducer';
import { useInjectSaga } from 'utils/injectSaga';
import {
  makeSelectRepos,
  makeSelectLoading,
  makeSelectError,
} from 'containers/App/selectors';
import HeadLiner from 'components/HeadLiner';
import Img from 'components/Img';
import Card from 'components/Card';
import HighlightedCard from 'components/HighlightedCard';
import Deck from 'components/Deck';
import Button from 'components/Button';
import NewsCard from 'components/NewsCard';
import { GiLaurelsTrophy } from 'react-icons/gi';
import { loadRepos } from '../App/actions';
import { changeUsername } from './actions';
import { makeSelectUsername } from './selectors';
import reducer from './reducer';
import saga from './saga';
import Map from './map.png';

GiLaurelsTrophy;

const key = 'home';

const ActivityCard = styled.div`
  background: #1a2739;
  border: 1px #1e2a3c solid;
  padding: 16px;
  color: #fff;
  border-radius: 8px;
  font-family: 'Segoe UI', Frutiger, 'Frutiger Linotype', 'Dejavu Sans',
    'Helvetica Neue', Arial, sans-serif;
  margin: 18px 18px;

  p {
    font-family: 'Segoe UI', Frutiger, 'Frutiger Linotype', 'Dejavu Sans',
      'Helvetica Neue', Arial, sans-serif;
  }
`;

const ProfileCard = styled.div`
  background: #1a2739;
  color: #fff;
  border-radius: 8px;
  font-family: 'Segoe UI', Frutiger, 'Frutiger Linotype', 'Dejavu Sans',
    'Helvetica Neue', Arial, sans-serif;
  margin: 6px;
  text-align: center;
  p {
    font-family: 'Segoe UI', Frutiger, 'Frutiger Linotype', 'Dejavu Sans',
      'Helvetica Neue', Arial, sans-serif;
  }
  img {
    width: 30px;
    height: 30px;
    border-radius: 50%;
    margin-right: 6px;
  }
`;

export function HomePage({
  username,
  loading,
  error,
  repos,
  onSubmitForm,
  onChangeUsername,
}) {
  useInjectReducer({ key, reducer });
  useInjectSaga({ key, saga });

  useEffect(() => {
    // When initial state username is not null, submit the form to load repos
    if (username && username.trim().length > 0) onSubmitForm();
  }, []);

  const reposListProps = {
    loading,
    error,
    repos,
  };
  return (
    <article style={{ flex: '1' }}>
      <Helmet>
        <title>Home</title>
        <meta
          name="description"
          content="A React.js Boilerplate application homepage"
        />
      </Helmet>
      <HeadLiner>Home</HeadLiner>
      <Deck>
        <HighlightedCard
          title="Tournaments"
          prizePool="221.073"
          type="tournaments"
          image="https://www.expertinvestor.net/images/heros/boe-tournaments.jpg"
        >
          Compete in tournaments to climb the scoreboard and win huge cash
          prizes. Join the action now for free. Atleast two tournaments a day,
          at 12:00 & 17:00 CET. Climb levels playing tournaments to get more
          points in Coin League.
        </HighlightedCard>
        <HighlightedCard
          title="Coin League"
          prizePool="5000"
          type="league"
          image="https://i.imgur.com/Fa243zR.png"
        >
          Play our monthly Coin League to be apart of a huge prize pool. Play
          with your monthly coins and the one with the most, grabs the insane
          price. This is a ongoing competition, that you can play whenever
          wherever.
        </HighlightedCard>
      </Deck>
      <HeadLiner>Recent Events</HeadLiner>
      <Deck>
        <ActivityCard>
          <ProfileCard>
            <Img src="https://www.voanews.com/themes/custom/voa/images/Author__Placeholder.png" />
            <div style={{ paddingBottom: '12px' }}>
              <small>SirPontusSpins</small>
            </div>
            <div style={{ borderTop: '1px #1E2A3C solid', paddingTop: '12px' }}>
              Just won 1123x on Lost Relics
            </div>
            <div style={{ paddingTop: '12px' }}>
              <Button
                type="info"
                style={{ flex: 1, marginTop: '18px' }}
                href="/tournaments/"
              >
                Congratulate
              </Button>
            </div>
          </ProfileCard>
        </ActivityCard>
        <ActivityCard>
          <ProfileCard>
            <Img src="https://www.voanews.com/themes/custom/voa/images/Author__Placeholder.png" />
            <div style={{ paddingBottom: '12px' }}>
              <small>SirPontusSpins</small>
            </div>
            <div style={{ borderTop: '1px #1E2A3C solid', paddingTop: '12px' }}>
              Just won 1123x on Lost Relics
            </div>
            <div style={{ paddingTop: '12px' }}>
              <Button
                type="info"
                style={{ flex: 1, marginTop: '18px' }}
                href="/tournaments/"
              >
                Congratulate
              </Button>
            </div>
          </ProfileCard>
        </ActivityCard>
        <ActivityCard>
          <ProfileCard>
            <Img src="https://www.voanews.com/themes/custom/voa/images/Author__Placeholder.png" />
            <div style={{ paddingBottom: '12px' }}>
              <small>SirPontusSpins</small>
            </div>
            <div style={{ borderTop: '1px #1E2A3C solid', paddingTop: '12px' }}>
              Just won 1123x on Lost Relics
            </div>
            <div style={{ paddingTop: '12px' }}>
              <Button
                type="info"
                style={{ flex: 1, marginTop: '18px' }}
                href="/tournaments/"
              >
                Congratulate
              </Button>
            </div>
          </ProfileCard>
        </ActivityCard>
        <ActivityCard>
          <ProfileCard>
            <Img src="https://www.voanews.com/themes/custom/voa/images/Author__Placeholder.png" />
            <div style={{ paddingBottom: '12px' }}>
              <small>SirPontusSpins</small>
            </div>
            <div style={{ borderTop: '1px #1E2A3C solid', paddingTop: '12px' }}>
              Just won 1123x on Lost Relics
            </div>
            <div style={{ paddingTop: '12px' }}>
              <Button
                type="info"
                style={{ flex: 1, marginTop: '18px' }}
                href="/tournaments/"
              >
                Congratulate
              </Button>
            </div>
          </ProfileCard>
        </ActivityCard>
      </Deck>
      <HeadLiner>Upcoming Tournaments</HeadLiner>
      <Deck>
        <Card
          title="Jungle Spirit"
          prizePool="1250"
          gameMode="score"
          image="https://i.imgur.com/CfeDwec.png"
        />
        <Card
          title="Koi Princess"
          prizePool="250"
          gameMode="multiplier"
          image="https://i.imgur.com/kQ6ZIIb.png"
        />
        <Card
          title="Narcos"
          prizePool="500"
          gameMode="score"
          image="https://i.imgur.com/7GBQRUM.png"
        />
        <Card
          title="Reel Rush"
          prizePool="3000"
          gameMode="multiplier"
          image="https://i.imgur.com/VukN0g6.png"
        />
      </Deck>
      <HeadLiner>Latest News</HeadLiner>
      <Deck>
        <NewsCard
          title="Growing fast and rapidly, extending..."
          image="https://activeops.com/wp-content/uploads/2017/01/ActiveOps-0276-752x500.jpg"
        />
        <NewsCard
          title="First local game station, close to you..."
          image="https://activeops.com/wp-content/uploads/2017/01/ActiveOps-0276-752x500.jpg"
        />
        <NewsCard
          title="Release 1.0 with a lot new features..."
          image="https://activeops.com/wp-content/uploads/2017/01/ActiveOps-0276-752x500.jpg"
        />
      </Deck>
    </article>
  );
}

HomePage.propTypes = {
  loading: PropTypes.bool,
  error: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
  repos: PropTypes.oneOfType([PropTypes.array, PropTypes.bool]),
  onSubmitForm: PropTypes.func,
  username: PropTypes.string,
  onChangeUsername: PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
  repos: makeSelectRepos(),
  username: makeSelectUsername(),
  loading: makeSelectLoading(),
  error: makeSelectError(),
});

export function mapDispatchToProps(dispatch) {
  return {
    onChangeUsername: evt => dispatch(changeUsername(evt.target.value)),
    onSubmitForm: evt => {
      if (evt !== undefined && evt.preventDefault) evt.preventDefault();
      dispatch(loadRepos());
    },
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(HomePage);
