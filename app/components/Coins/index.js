/**
 *
 * Img.js
 *
 * Renders an image, enforcing the usage of the alt="" tag
 */

import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Input from './Input'

import Img from '../Img'
import CoinImage from  './coin.png';

const CoinsContainer = styled.div`
  color:#FFF;

  img{
    width:25px;
    height:25px;
  }
  b{
    margin-left:6px;
  }
`;



function Coin(props) {
  return (
    <CoinsContainer>
      <Img src={CoinImage}></Img> <b>{props.amount}</b>
    </CoinsContainer>
  );
}

// We require the use of src and alt, only enforced by react in dev mode
Coin.propTypes = {
  amount: PropTypes.string.isRequired
};

export default Coin;
