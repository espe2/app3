import React, { Children } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { MdKeyboardArrowRight } from 'react-icons/md';
import Img from '../Img';
import Button from '../Button';

const CardHolder = styled.div`
  border: 1px #1e2a3c solid;
  background: #152335;
  border-radius: 8px;
  margin: 0px 18px 18px 18px;
  flex: 1;
  overflow: hidden;
  font-family: 'Segoe UI', Frutiger, 'Frutiger Linotype', 'Dejavu Sans',
    'Helvetica Neue', Arial, sans-serif;
  cursor: pointer;
  img {
    width: 100%;
    border-radius: 8px;
  }
  &:hover {
    background: #132030;
  }
`;

const InfoBox = styled.div`
  display: flex;
  flex: 1;
  flex-direction: row;
  justify-content: flex-start;
  margin-top: -64px;

  a {
    margin: 22px 12px 12px 12px;
  }
  div {
    /* Permalink - use to edit and share this gradient: https://colorzilla.com/gradient-editor/#000000+1,000000+100&0+0,0.65+100 */
    background: -moz-linear-gradient(
      top,
      rgba(0, 0, 0, 0) 0%,
      rgba(0, 0, 0, 0.01) 1%,
      rgba(0, 0, 0, 0.65) 100%
    ); /* FF3.6-15 */
    background: -webkit-linear-gradient(
      top,
      rgba(0, 0, 0, 0) 0%,
      rgba(0, 0, 0, 0.01) 1%,
      rgba(0, 0, 0, 0.65) 100%
    ); /* Chrome10-25,Safari5.1-6 */
    background: linear-gradient(
      to bottom,
      rgba(0, 0, 0, 0) 0%,
      rgba(0, 0, 0, 0.01) 1%,
      rgba(0, 0, 0, 0.65) 100%
    ); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#00000000', endColorstr='#a6000000',GradientType=0 ); /* IE6-9 */
  }
`;

const Title = styled.div`
  font-size: 16px;
  color: #fff;
  flex: 1;
  text-shadow: 0px 0px 20px rgba(0, 0, 0, 0.84) !important;
  padding: 22px 12px 12px 12px;
`;

function NewsCard(props) {
  return (
    <CardHolder>
      <Img src={props.image} alt="Image" />
      <InfoBox>
        <Title>{props.title}</Title>
        <Button type="info" href="/features">
          Read <MdKeyboardArrowRight style={{ fontSize: '18px' }} />
        </Button>
      </InfoBox>
    </CardHolder>
  );
}

NewsCard.propTypes = {
  title: PropTypes.string.isRequired,
  image: PropTypes.string.isRequired,
};

export default NewsCard;
