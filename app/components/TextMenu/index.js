import React from 'react';
import PropTypes from 'prop-types';

import { withRouter } from 'react-router-dom';
import Menu from './Menu';
import Item from './Item';
import Wrapper from './Wrapper';

function TextMenu(props) {
  console.log(props.location.pathname);
  return (
    <Wrapper>
      <Menu>
        <Item
          href="/"
          className={props.location.pathname == '/' ? 'active' : ''}
        >
          Home
        </Item>
        <Item
          href="/tournaments"
          className={props.location.pathname == '/tournaments' ? 'active' : ''}
        >
          Tournaments
        </Item>
        <Item
          href="/league"
          className={props.location.pathname == '/league' ? 'active' : ''}
        >
          Coin League
        </Item>
        <Item
          href="/bonuses"
          className={props.location.pathname == '/bonuses' ? 'active' : ''}
        >
          Bonuses
        </Item>
        <Item
          href="/news"
          className={props.location.pathname == '/news' ? 'active' : ''}
        >
          News
        </Item>
        <Item>Guides</Item>
        <Item>Leaderboards</Item>
      </Menu>
    </Wrapper>
  );
}

export default withRouter(TextMenu);
