import styled from 'styled-components';

const Menu = styled.div`
    flex-direction: column;
    display: flex;
    flex: 1;
    color:#FFF;
    img{
        width:32px;
        height:32px;
        border-radius:50%;
    }
`;

export default Menu;
