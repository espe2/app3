import { css } from 'styled-components';

const buttonInfo = css`
  display: inline-block;
  box-sizing: border-box;
  padding: 0.25em 2em;
  text-decoration: none;
  border-radius: 12px;
  -webkit-font-smoothing: antialiased;
  -webkit-touch-callout: none;
  user-select: none;
  cursor: pointer;
  outline: 0;
  font-family: 'Segoe UI', Frutiger, 'Frutiger Linotype', 'Dejavu Sans',
    'Helvetica Neue', Arial, sans-serif;
  font-weight: bold;
  font-size: 12px;
  border: 2px solid #226ef2;
  color: #fff;
  background-color: #226ef2;

  &:active {
    background: #226ef2;
    color: #fff;
  }
  &:hover {
    background: #1c60d6;
    color: #fff;
  }
`;

export default buttonInfo;
