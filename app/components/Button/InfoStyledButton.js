import styled from 'styled-components';

import buttonInfo from './buttonInfo';

const InfoStyledButton = styled.button`
  ${buttonInfo};
`;

export default InfoStyledButton;
