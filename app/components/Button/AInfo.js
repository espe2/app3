import styled from 'styled-components';

import buttonInfo from './buttonInfo';

const AInfo = styled.a`
  ${buttonInfo};
`;

export default AInfo;
