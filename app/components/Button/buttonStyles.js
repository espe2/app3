import { css } from 'styled-components';

const buttonStyles = css`
  display: inline-block;
  box-sizing: border-box;
  padding: 0.25em 2em;
  text-decoration: none;
  border-radius: 12px;
  -webkit-font-smoothing: antialiased;
  -webkit-touch-callout: none;
  user-select: none;
  cursor: pointer;
  outline: 0;
  font-family: "Segoe UI", Frutiger, "Frutiger Linotype", "Dejavu Sans", "Helvetica Neue", Arial, sans-serif;
  font-weight: bold;
  font-size: 12px;
  border: 2px solid #FFB700;
  color: #FFF;
  background-color: #FFB700;

  &:active {
    background: #FFB700;
    color: #fff;
  }
  &:hover {
    background: #e8a700;
    color: #fff;
  }
`;

export default buttonStyles;
