/**
 *
 * Button.js
 *
 * A common button, if you pass it a prop "route" it'll render a link to a react-router route
 * otherwise it'll render a link with an onclick
 */

import React, { Children } from 'react';
import PropTypes from 'prop-types';

import A from './A';
import AInfo from './AInfo';
import StyledButton from './StyledButton';
import InfoStyledButton from './InfoStyledButton';
import Wrapper from './Wrapper';

function Button(props) {
  // Render an anchor tag
  let button = '';
  switch (props.type) {
    case 'info':
      button = (
        <AInfo href={props.href} onClick={props.onClick}>
          {Children.toArray(props.children)}
        </AInfo>
      );
      break;
    default:
      button = (
        <A href={props.href} onClick={props.onClick}>
          {Children.toArray(props.children)}
        </A>
      );
      break;
  }

  // If the Button has a handleRoute prop, we want to render a button
  if (props.handleRoute) {
    switch (props.type) {
      case 'info':
        button = (
          <InfoStyledButton onClick={props.handleRoute}>
            {Children.toArray(props.children)}
          </InfoStyledButton>
        );
        break;
      default:
        button = (
          <StyledButton onClick={props.handleRoute}>
            {Children.toArray(props.children)}
          </StyledButton>
        );
        break;
    }
  }

  return <Wrapper>{button}</Wrapper>;
}

Button.propTypes = {
  handleRoute: PropTypes.func,
  href: PropTypes.string,
  onClick: PropTypes.func,
  children: PropTypes.node.isRequired,
  type: PropTypes.string,
};

export default Button;
