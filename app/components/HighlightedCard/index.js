import React, { Children } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { MdKeyboardArrowRight } from 'react-icons/md';
import Img from '../Img';
import Button from '../Button';
import A from '../A';

const CardHolder = styled.div`
  border: 1px #1e2a3c solid;
  padding: 2px 2px;
  background: #152335;
  border-radius: 8px;
  margin: 0px 18px;
  flex: 1;
  overflow: hidden;
  font-family: 'Segoe UI', Frutiger, 'Frutiger Linotype', 'Dejavu Sans',
    'Helvetica Neue', Arial, sans-serif;
  cursor: pointer;
  img {
    width: 100%;
    border-radius: 8px;
  }
  &:hover {
    background: #132030;
  }
`;

const InfoBox = styled.div`
  padding: 12px 12px 12px 12px;
  display: flex;
  flex: 1;
  flex-direction: row;
  justify-content: flex-start;
`;

const Title = styled.span`
  font-size: 32px;
  color: #fff;
  flex: 1;
`;

const SubTitle = styled.span`
  font-size:14px;
  flex:1;
  color:#FFF;
  text-align:center;
  p{
    font-family: "Segoe UI", Frutiger, "Frutiger Linotype", "Dejavu Sans", "Helvetica Neue", Arial, sans-serif !important;
    font:size:14px;
    color:#A4A4A4;
    margin-bottom:2px;q
  }
`;

const Live = styled.div`
  width: 12px;
  height: 12px;
  background-color: #169d3a;
  border-radius: 50%;
  float: right;
  margin-bottom: -42px;
  margin-right: 22px;
`;

function InfoBoxFeed(props) {
  switch (props.type) {
    case 'tournaments':
      return (
        <InfoBox style={{ borderTop: '1px #1E2A3C solid' }}>
          <SubTitle style={{ borderRight: '1px #1E2A3C solid' }}>
            <p>Paid Out</p>
            <span style={{ color: '#FFB700' }}>€{props.prizePool}</span>
          </SubTitle>
          <SubTitle style={{ borderRight: '1px #1E2A3C solid' }}>
            <p>Live</p>
            <span>5 Tournaments</span>
          </SubTitle>
          <SubTitle>
            <p>Last Winner</p>
            <span>SirPontusSpins</span>
          </SubTitle>
        </InfoBox>
      );
      break;
    case 'league':
      return (
        <InfoBox style={{ borderTop: '1px #1E2A3C solid' }}>
          <SubTitle style={{ borderRight: '1px #1E2A3C solid' }}>
            <p>Prize Pool</p>
            <span style={{ color: '#FFB700' }}>€{props.prizePool}</span>
          </SubTitle>
          <SubTitle style={{ borderRight: '1px #1E2A3C solid' }}>
            <p>Players</p>
            <span>22.631</span>
          </SubTitle>
          <SubTitle>
            <p>Leader</p>
            <span>SirPontusSpins</span>
          </SubTitle>
        </InfoBox>
      );
      break;
  }
}

function ButtonFeed(props) {
  switch (props.type) {
    case 'tournaments':
      return (
        <div>
          <Button style={{ flex: 1, marginTop: '18px' }} href="/tournaments/">
            Play Now <MdKeyboardArrowRight style={{ fontSize: '18px' }} />
          </Button>
        </div>
      );
      break;
    case 'league':
      return (
        <div>
          <Button style={{ flex: 1, marginTop: '18px' }} href="/league/">
            Join Now <MdKeyboardArrowRight style={{ fontSize: '18px' }} />
          </Button>
        </div>
      );
      break;
  }
}
function HighlightedCard(props) {
  return (
    <CardHolder>
      <Live />
      <Img src={props.image} alt="Image" />
      <InfoBox>
        <Title>{props.title}</Title>
        {ButtonFeed(props)}
      </InfoBox>
      <InfoBox>
        <SubTitle style={{ textAlign: 'left' }}>
          {Children.toArray(props.children)}
        </SubTitle>
      </InfoBox>
      {InfoBoxFeed(props)}
    </CardHolder>
  );
}

HighlightedCard.propTypes = {
  title: PropTypes.string.isRequired,
  prizePool: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  image: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired,
};

export default HighlightedCard;
