import React, { Children } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const DeckHolder = styled.div`
  height:120px;
  background:#000;
  width:100%;
  position:fixed;
  bottom:0;
  left:0;
  right:0;
  z-index:999;
`;

const EventWrapper = styled.span`
  border: 1px #1E2A3C solid; 
  padding:2px 2px;
  background:#152335;
  border-radius:8px;
  color:#FFF;
  padding:6px;
  font-size:12px;
  position:fixed;
  bottom:0;
`;

function EventFeed(props) {
  return (
    <div></div>
  );
}

EventFeed.propTypes = {
  children: PropTypes.node.isRequired,
};


export default EventFeed;
