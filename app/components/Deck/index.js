import React, { Children } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const DeckHolder = styled.div`
  display: flex;
  flex: 1;
  flex-direction: row;
  justify-content: flex-start;
`;

function Deck(props) {
  return (
    <DeckHolder>
       {Children.toArray(props.children)}
    </DeckHolder>
  );
}

Deck.propTypes = {
  children: PropTypes.node.isRequired,
};


export default Deck;
