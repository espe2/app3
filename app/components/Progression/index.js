import React, { Children } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const ProgressionWrapper = styled.div`
  border-radius:8px;
  background:#1B2738;
  border:1px #1E2A3C solid;
  width:100%;
  text-align:center;
  height:24px;
`;

const ProgressionUI = styled.div`
  background:#FFB700;
  padding:3px;
  color:#FFF;
  border-radius:8px;
  font-size:11px;
  span{
    margin: 0 auto;
    text-align:center;
  }
`;

function Progression(props) {
  return (
    <ProgressionWrapper>
      <ProgressionUI style={{width: (props.percentage+"%"), textAlign: "center"}}>
        <span>{props.percentage}%</span>
      </ProgressionUI>
    </ProgressionWrapper>
  );
}

Progression.propTypes = {
  percentage: PropTypes.string.isRequired,
};


export default Progression;
