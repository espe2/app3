import styled from 'styled-components';

const SlimCard = styled.div`
    background:#1A2739;
    border:1px #1E2A3C solid;
    padding:16px;
    color:#FFF;
    border-radius:8px;
    font-family: "Segoe UI", Frutiger, "Frutiger Linotype", "Dejavu Sans", "Helvetica Neue", Arial, sans-serif;
    margin:6px;

    p{
    font-family: "Segoe UI", Frutiger, "Frutiger Linotype", "Dejavu Sans", "Helvetica Neue", Arial, sans-serif;
    }
`;

export default SlimCard;