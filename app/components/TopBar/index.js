import React from 'react';
import { FormattedMessage } from 'react-intl';

import styled from 'styled-components';
import A from './A';
import Img from './Img';
import HeaderLink from './HeaderLink';
import Logo from './logo.png';
import Container from './Container';
import Search from '../Search';
import Coins from '../Coins';
import Progression from '../Progression';

const ProfileCard = styled.div`
  background: #1a2739;
  border: 1px #1e2a3c solid;
  padding: 6px;
  color: #fff;
  border-radius: 8px;
  font-family: 'Segoe UI', Frutiger, 'Frutiger Linotype', 'Dejavu Sans',
    'Helvetica Neue', Arial, sans-serif;
  margin: 6px;
  flex-direction: row;
  display: flex;

  span {
    font-family: 'Segoe UI', Frutiger, 'Frutiger Linotype', 'Dejavu Sans',
      'Helvetica Neue', Arial, sans-serif;
  }
  img {
    width: 30px;
    height: 30px;
    border-radius: 50%;
    margin-right: 6px;
  }
`;

function TopBar() {
  return (
    <div>
      <Container>
        <div style={{ width: '280px', borderRight: '1px #1E2A3C solid' }}>
          <Img src={Logo} />
        </div>
        <div flex="25" style={{ padding: '16px 16px', flex: 1 }}>
          <Search placeholder="Search for games, tournaments etc" />
        </div>
        <div flex="25" style={{ padding: '26px 16px' }}>
          <Coins amount="255" />
        </div>
        <div
          flex="25"
          style={{
            padding: '12px 22px',
            width: '120px',
            flexDirection: 'column',
            display: 'flex',
            textAlign: 'center',
          }}
        >
          <b style={{ flex: 1, color: '#FFB700' }}>LVL 23</b>
          <div style={{ flex: 1 }}>
            <Progression percentage="74" />
          </div>
        </div>
        <div flex="25" style={{ padding: '12px 16px' }}>
          <ProfileCard>
            <Img src="https://www.voanews.com/themes/custom/voa/images/Author__Placeholder.png" />
            <span>SirPontusSpins</span>
          </ProfileCard>
        </div>
      </Container>
    </div>
  );
}

export default TopBar;
