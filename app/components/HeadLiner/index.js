import React, { Children } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import H3 from '../H3';

const Box = styled.div`
  padding:2px 22px;
  flex:1;
  display:flex;
  color:#FFF;
  margin-top:-1px;

  h2{
    margin:2px !important;
  }
`;

function HeadLiner(props) {
  return (
    <Box>
      <H3>{Children.toArray(props.children)}</H3>
    </Box>
  );
}

HeadLiner.propTypes = {
  children: PropTypes.node.isRequired,
};


export default HeadLiner;
